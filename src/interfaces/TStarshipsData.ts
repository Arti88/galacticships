import { Ship } from '@/classes/Ship';

export interface TStarshipsData {
  count: number;
  next?: string;
  previous?: string;
  results: Ship[];
}
