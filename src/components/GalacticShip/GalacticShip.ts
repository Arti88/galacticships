import { Component, Vue } from 'vue-property-decorator';
import WithRender from './galactic-ship.html';
import {Ship} from '@/classes/Ship';
import shipsService from '@/services/ShipsService';

@WithRender
@Component
export default class GalacticShip extends Vue {
    protected ship!: Ship|null;
    protected isLoading: boolean = true;
    protected async mounted() {
        const { shipID } = this.$route.params;
        try {
            this.ship = await shipsService.getByID(Number(shipID));
        } catch (e) {
            this.ship = null;
        } finally {
            this.isLoading = false;
        }
    }
}
