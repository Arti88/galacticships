import { Component, Vue } from 'vue-property-decorator';
import WithRender from './galactic-ships-list.html';
import shipsService from '@/services/ShipsService';
import {Ship} from '@/classes/Ship';

@WithRender
@Component
export default class GalacticShipsList extends Vue {
    protected ships: Ship[] = [];
    protected page: number = 1;
    protected previousPage: number|null = null;
    protected nextPage: number|null = null;
    protected search: string = '';
    protected searchQuery: string = '';
    protected count: number = 0;
    protected isLoading: boolean = false;
    protected async mounted() {
        await this.showResults(1);
    }
    protected showPreviousPage(): void {
        this.showResults(this.page - 1);
    }
    protected showNextPage() {
        this.showResults(this.page + 1);
    }

    protected clearSearchResults() {
        this.search = '';
        this.showResults(1);
    }

    private async showResults(pageNumber: number = this.page || 1) {
        if (this.isLoading) {
            return;
        }
        try {
            this.isLoading = true;
            const { searchQuery, starshipsData } = await shipsService.getAll(pageNumber, this.search.trim());
            this.ships = starshipsData.results;
            this.searchQuery = searchQuery;
            this.count = starshipsData.count;
            this.page = (starshipsData.next
                ? Number(starshipsData.next) - 1
                : Number(starshipsData.previous) + 1) || 1;
            this.previousPage = starshipsData.previous
                ? Number((new URL(starshipsData.previous)).searchParams.get('page'))
                : null;
            this.nextPage = starshipsData.next
                ? Number((new URL(starshipsData.next)).searchParams.get('page'))
                : null;
            this.page = pageNumber;
        } catch (ignored) {
            alert(ignored.message);
        } finally {
            this.isLoading = false;
        }
    }
}
