import Vue from 'vue';
import Router from 'vue-router';
import GalacticShipsList from '@/components/GalacticShipsList/GalacticShipsList';
import GalacticShip from '@/components/GalacticShip/GalacticShip';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Galactic ships list',
      component: GalacticShipsList,
    },
    {
      path: '/ship/:shipID',
      name: 'DetailedShipInformation',
      component: GalacticShip,
    },
  ],
});
