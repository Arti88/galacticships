import axios from 'axios';

const axiosConfig = axios.create({
  baseURL: 'https://swapi.co/api/starships/',
  method: 'GET',
});

export default axiosConfig;
