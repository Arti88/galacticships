import axiosConfig from '@/constants/axiousConfig.constant';

import {Ship} from '@/classes/Ship';
import {TStarshipsData} from '@/interfaces/TStarshipsData';
import {TShipData} from '@/interfaces/TShipData';
import {AxiosResponse} from 'axios';
import {Dictionary} from 'vue-router/types/router';

class ShipsService {
  private cachedShipsByPage: Dictionary<any> = {};
  public getAll(
    page: number,
    search: string,
  ): Promise<{ starshipsData: TStarshipsData, searchQuery: string }> {
    if (this.cachedShipsByPage[String(page)] && !search) {
      return Promise.resolve({
        starshipsData: this.cachedShipsByPage[String(page)],
        searchQuery: '',
      });
    }
    const queryPart = `?page=${page}${search ? `&search=${search}` : ''}`;
    return axiosConfig.get(queryPart)
      .then((response: AxiosResponse<TStarshipsData>) => {
        const starshipsData = Object.assign(
            response.data,
            { results: response.data.results.map((shipData: any) => Ship.createFromShipData(shipData)) },
        );
        const result = {
          starshipsData,
          searchQuery: '',
        };
        if (!search) {
          this.cachedShipsByPage[`${page}`] = starshipsData;
          return result;
        }
        result.searchQuery = `${axiosConfig.defaults.baseURL}${queryPart}`;
        return result;
      });
  }

  public getByID(id: number): Promise<Ship|null> {
    return axiosConfig.get(`${id}`)
      .then(
          (response: AxiosResponse<TShipData>) => Ship.createFromShipData(response.data),
          () => null);
  }
}

const shipsService = new ShipsService();
export default shipsService;

