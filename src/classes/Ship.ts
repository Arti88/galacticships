import {TShipData} from '@/interfaces/TShipData';

// tslint:disable:variable-name
export class Ship {

  get name(): string {
    return this._name;
  }

  get model(): string {
    return this._model;
  }

  get manufacturer(): string {
    return this._manufacturer;
  }

  get costInCredits(): number {
    return this._cost_in_credits;
  }

  get length(): number {
    return this._length;
  }

  get maxAtmoshperingSpeed(): number {
    return this._max_atmosphering_speed;
  }

  get crew(): number {
    return this._crew;
  }

  get passengers(): number {
    return this._passengers;
  }

  get cargoCapacity(): number {
    return this._cargo_capacity;
  }

  get consumables(): string {
    return this._consumables;
  }

  get hyperdriveRating(): number {
    return this._hyperdrive_rating;
  }

  get mglt(): number {
    return this._MGLT;
  }

  get starshipClass(): string {
    return this._starship_class;
  }

  get pilots(): string[] {
    return this._pilots;
  }

  get films(): string[] {
    return this._films;
  }

  get created(): string {
    return this._created;
  }

  get edited(): string {
    return this._edited;
  }

  get url(): string {
    return this._url;
  }

  get id(): number {
    return this._id;
  }

  public static createFromShipData(data: any): Ship {
    return new Ship(
      data.name,
      data.model,
      data.manufacturer,
      data.cost_in_credits,
      data.length,
      data.max_atmosphering_speed,
      data.crew,
      data.passengers,
      data.cargo_capacity,
      data.consumables,
      data.hyperdrive_rating,
      data.MGLT,
      data.starship_class,
      data.pilots,
      data.films,
      data.created,
      data.edited,
      data.url,
      Ship.getIDFromUrl(data.url),
    );
  }

  private static getIDFromUrl(url: string): number {
    return Number.parseInt(`${url.match(/(\d+)\/$/g)}`, 10);
  }
  constructor(
    private _name: string,
    private _model: string,
    private _manufacturer: string,
    private _cost_in_credits: number,
    private _length: number,
    private _max_atmosphering_speed: number,
    private _crew: number,
    private _passengers: number,
    private _cargo_capacity: number,
    private _consumables: string,
    private _hyperdrive_rating: number,
    private _MGLT: number,
    private _starship_class: string,
    private _pilots: string[],
    private _films: string[],
    private _created: string,
    private _edited: string,
    private _url: string,
    private _id: number,
  ) {}
}
